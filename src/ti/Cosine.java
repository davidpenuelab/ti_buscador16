package ti;

import java.util.*;
import java.util.Map.Entry;

/**
 * Implements retrieval in a vector space with the cosine similarity function and a TFxIDF weight formulation.
 */
public class Cosine implements RetrievalModel
{
	public static int i = 0;
	public Cosine()
	{
		// vacío
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArrayList<Tuple<Integer, Double>> runQuery(String queryText, Index index, DocumentProcessor docProcessor)
	{
		// P1
		
		// extraer términos de la consulta
		ArrayList <String> terminosQueryAL = docProcessor.processText(queryText); // tenemos los terminos que nos interesan de la consulta.

		// calcular el vector consulta
		ArrayList<Tuple<Integer, Double>> vectorConsulta = this.computeVector(terminosQueryAL, index);
		
		return this.computeScores(vectorConsulta, index); // devuelve una lista con la similitud de cada codumento con la consulta
//		return null; // devolver resultados
	}

	/**
	 * Returns the list of documents in the specified index sorted by similarity with the specified query vector.
	 *
	 * @param queryVector the vector with query term weights.
	 * @param index       the index to search in.
	 * @return a list of {@link Tuple}s where the first item is the {@code docID} and the second one the similarity score.
	 */
	protected ArrayList<Tuple<Integer, Double>> computeScores(ArrayList<Tuple<Integer, Double>> queryVector, Index index)
	{
		ArrayList<Tuple<Integer, Double>> results = new ArrayList<>();
		// P1
				/*
				 * 
				 * la norma se calcula por cada documento. norma = raiz cuadrada del
				 * sumatario de los pesos al cuadrado // sqrt(sum(Wi^2)) donde Wi es el
				 * 	peso de cada termino en un documento. para cada termino del vector
				 * 	consulta(vectorConsulta<termID,Wtq>): buscar el termino en
				 * 	invertedIndex para que nos devuelva la tupla <docID, Wtd>
				 * 		result[docId]+=wtq
				 */
		HashMap<Integer, Double> sims = new HashMap<>();
		double queryNorm = 0;
		for (Tuple<Integer,Double> qTerm : queryVector){
			int termID = qTerm.item1;
			double qW = qTerm.item2;
			for (Tuple<Integer, Double> posting : index.invertedIndex.get(termID)){
				int docID = posting.item1;
				double dw = posting.item2;
				
				Double docScore = sims.get(docID);
				if (docScore == null)
					docScore = 0d;
				sims.put(docID, docScore+dw*qW);
			}
			queryNorm += qW*qW;
		}
		queryNorm = Math.sqrt(queryNorm);
		
		for(Map.Entry<Integer, Double> sim : sims.entrySet()){
			int docID = sim.getKey();
			double simScore = sim.getValue();
			double docNorm = index.documents.get(docID).item2;
			results.add(new Tuple<>(docID, simScore/queryNorm/docNorm));
		}
		
		Collections.sort(results, new Comparator<Tuple<Integer, Double>>()
		{
			@Override
			public int compare(Tuple<Integer, Double> o1, Tuple<Integer, Double> o2)
			{
				return o2.item2.compareTo(o1.item2);
			}
		});
//		System.out.println("HERE5");

		return results;
	}

	/**
	 * Compute the vector of weights for the specified list of terms.
	 *
	 * @param terms the list of terms.
	 * @param index the index
	 * @return a list of {@code Tuple}s with the {@code termID} as first item and the weight as second one.
	 */
	protected ArrayList<Tuple<Integer, Double>> computeVector(ArrayList<String> terms, Index index)
	{
		ArrayList<Tuple<Integer, Double>> resultFinal = new ArrayList<>();

		// P1
		/*
		 * Para cada term:
		 * 	1. contar cuantas veces aparece en la consulta (para hallar  el tf =  (1+log(ftq)) )
		 *  2. buscar el term en el vocabulario. Si aparacere, extraer el TermID y el IDF
		 *  3. guardar term id y peso (w = tf*df) en el vector
		 * 
		 * */
		HashSet<String> uniqTerms = new HashSet<>(terms);
		for (String term : uniqTerms){
			Tuple<Integer,Double> termInfo = index.vocabulary.get(term);
			if (termInfo != null){
				int termID = termInfo.item1;
				double idf = termInfo.item2;
				double tf = 1.0 + Math.log(Collections.frequency(terms, term));
				
				resultFinal.add(new Tuple<>(termID, tf*idf));
			}
		}
		return resultFinal;
	}
	
	int contains(ArrayList<Tuple<Integer, Double>> list, int termID) {
	    int i = 0;
		for (Tuple<Integer, Double> item : list) {
	        if (item.item1.equals(termID)) {
	            return i;
	        }
	        i++;
	    }
	    return -1;
	}
}
