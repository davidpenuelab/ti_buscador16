package ti;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class contains the logic to run the retrieval process of the search engine in interactive mode.
 */
public class Interactive
{
	protected RetrievalModel model;
	protected Index index;
	protected DocumentProcessor docProcessor;
	protected String queryFromUser;

	/**
	 * Creates a new interactive retriever using the given model.
	 *
	 * @param model        the retrieval model to run queries.
	 * @param index        the index.
	 * @param docProcessor the processor to extract query terms.
	 */
	public Interactive(RetrievalModel model, Index index, DocumentProcessor docProcessor)
	{
		this.model = model;
		this.index = index;
		this.docProcessor = docProcessor;
	}

	/**
	 * Runs the interactive retrieval process. It asks the user for a query, and then it prints the results to
	 * {@link System#out} showing the document title and a snippet, highlighting important terms for the query.
	 *
	 * @throws Exception in an error occurs during the process.
	 */
	public void run() throws Exception
	{
		// Run prompt loop
		Scanner scan = new Scanner(System.in);

		int from = 0;
		int selector = 0;
		ArrayList<Tuple<Integer, Double>> results = null;
		boolean waitingForQuery = true;
		System.out.print("Please, enter your query: \nIn order to quit, leave it empty and press enter\n\n > ");
		do {

			scan.reset();
			queryFromUser = scan.nextLine();

			if (queryFromUser.equals("")) System.exit(0);
			if(waitingForQuery){
				waitingForQuery = false;
				results = this.model.runQuery(queryFromUser, this.index, this.docProcessor);
				switch (selector) {
				//Primer paso en recibir una Query
					case 0:
						this.printResults(results, 0, 10);
						this.printChoice(0);
						break;
				//Aquí el tío va cambiando de página, etc
					case 1:
						this.printResults(results, from, 10);
						//Por si ha vuelto a la primera después de haber mirado otras
						if(from == 0)
							this.printChoice(0);
						else
							this.printChoice(1);
						break;
				//Modo ultra cutre de pasar el bucle sin hacer nada y resetearlo todo
					case 2:
						selector = 0;
						from = 0;
						break;
				}
			}else{
				waitingForQuery = true;
				//Quiere ir a la previa
				if(queryFromUser.equals("P")){	
					from = from - 10;
					selector = 1;
				}else if(queryFromUser.equals("N")){	
					//Quiere ir a la siguiente
					from = from + 10;
					selector = 1;
				}else if(queryFromUser.equals("Q")){
					//Quiere hacer una nueva Query
					selector = 2;
				}	
				this.printResults(results, from, 10);
			}
			
			
			
			/* COMENTARIOS Y CÓDIGO DADO */
			//ArrayList<Tuple<Integer, Double>> results = this.model.runQuery(input, this.index, this.docProcessor);

			// P5
			// paginar resultados
			
			//this.printResults(results, 0, 10);
		} while (!queryFromUser.isEmpty());
	}

	/**
	 * Print a page of results for a query, showing for each document its title and snippet, with highlighted terms.
	 *
	 * @param results the results for the query. A list of {@link Tuple}s where the first item is the {@code docID} and
	 *                the second item is the similarity score.
	 * @param from    index of the first result to print.
	 * @param count   how many results to print from the {@code from} index.
	 */
	protected void printResults(ArrayList<Tuple<Integer, Double>> results, int from, int count)
	{
		//P5
		int docID;
		String docTitle;
		System.out.println();
		
		for(int i = from; i<from+count; i++)
		{
			docID = results.get(i+from).item1;
			Tuple <String, String> currentDoc = null;
			try {
				currentDoc = index.getCachedDocument(docID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(i+" ("+docID+"): "+currentDoc.item1+"\n"+createSnipet(currentDoc.item2));
		}
	}
	
	protected String createSnipet(String body){
		String result = "";
		String[] termsFromQueryInArray = queryFromUser.split(" ");
		try{
			result = body.substring(body.indexOf(termsFromQueryInArray[0])-10,body.indexOf(termsFromQueryInArray[0])+300);			
		}catch (Exception e) {
			result = body.substring(100,300);			
		}
		
		//resaltar terminos aqui.
		for (String term : termsFromQueryInArray){
			result.replace(term,"*"+term+"*");
		}
		result = "..."+result+"...";
		return result;
	}
	protected void printChoice(int selector) 
	{
		switch (selector) {
		//Estamos en la primera página
			case 0:
				System.out.println();
				System.out.println("\t Next page enter 'N'");
				System.out.println("\t Previous page enter 'P'");
				break;
		//Estamos en páginas centrales
			case 1:
				System.out.println();
				System.out.println("\t Next page enter 'N'");
				System.out.println("\t Previous page enter 'P'");
				System.out.println("To do a new query, please enter 'Q'");
				break;
		//Habría que contemplar el último caso, en que el tío está en la última página
		//Pero es muy vaina detectarlo
		//Así que #sudando
		}
		System.out.println();
	}
}