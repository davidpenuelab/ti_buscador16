package ti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Implements retrieval in a vector space with the cosine similarity function and a TFxIDF weight formulation,
 * plus pseudorelevance feedback.
 */
public class CosineWithFeedback extends Cosine
{
	protected int feedbackDepth;
	protected double feedbackAlpha;
	protected double feedbackBeta;

	/**
	 * Creates a new retriver with the specified pseudorelevance feedback parameters.
	 *
	 * @param feedbackDepth number of documents to consider relevant.
	 * @param feedbackAlpha relative weight of the original query terms
	 * @param feedbackBeta  relative weight of the expanded terms.
	 */
	public CosineWithFeedback(int feedbackDepth, double feedbackAlpha, double feedbackBeta)
	{
		super();
		this.feedbackDepth = feedbackDepth;
		this.feedbackAlpha = feedbackAlpha;
		this.feedbackBeta = feedbackBeta;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArrayList<Tuple<Integer, Double>> runQuery(String queryText, Index index, DocumentProcessor docProcessor)
	{
		// P4
		// calcular resultados iniciales
			// extraer términos de la consulta
		ArrayList <String> allTermsInQuery = docProcessor.processText(queryText); // tenemos los terminos que nos interesan de la consulta.
			// calcular el vector consulta
		ArrayList<Tuple<Integer, Double>> vectorConsulta = super.computeVector(allTermsInQuery, index);
		
		ArrayList<Tuple<Integer, Double>> results =  super.computeScores(vectorConsulta, index); // devuelve una lista con la similitud de cada codumento con la consulta
		// actualizar vector consulta
		// volver a ejecutar conulta
		int rocchioTimes = 10;
		ArrayList<Tuple<Integer, Double>> modQuery = null;
		for (int i = 0; i < rocchioTimes; i++){
			modQuery = this.computeFeedbackVector(vectorConsulta, results, index);
			results.clear();
			results = super.computeScores(modQuery, index);
		}
		
//		return null;
		return results; // y devolver resultados
	}

	/**
	 * Computes the modified query vector for relevance feedback.
	 *
	 * @param queryVector the original query vector. (termID, weight)
	 * @param results     the results with the original query.(docID, similitud)
	 * @param index       the index to search in.
	 * @return a list of {@code Tuple}s with the {@code termID} as first item and the weight as second one.
	 */
	protected ArrayList<Tuple<Integer, Double>> computeFeedbackVector(ArrayList<Tuple<Integer, Double>> queryVector,
	                                                                  ArrayList<Tuple<Integer, Double>> results,
	                                                                  Index index)
	{
		ArrayList<Tuple<Integer, Double>> weights = new ArrayList<>();
		
		// P4
		HashMap <Integer, Double> qSumTerms = new HashMap<>();
		double betaR = feedbackBeta / feedbackDepth;
		
		int numberOfDocs = results.size();
		
		for (int j = 0; j < queryVector.size();j++){
			int termID = queryVector.get(j).item1;
			double peso = queryVector.get(j).item2;
			
			qSumTerms.put(termID,(feedbackAlpha*peso));
						
		}
		for (int i = 0; i < feedbackDepth && i< numberOfDocs; i++){
			
			int docID = results.get(i).item1;
			ArrayList<Tuple<Integer, Double>> termsOfDoc = index.directIndex.get(docID);
			
			for (int j = 0; j < termsOfDoc.size();j++){
				int termID = termsOfDoc.get(j).item1;
				double peso = termsOfDoc.get(j).item2;
				
				if(qSumTerms.containsKey(termID)){
					qSumTerms.put(termID, qSumTerms.get(termID)+(peso*betaR));
				}else{
					qSumTerms.put(termID,betaR*peso);
				}				
			}
			
		}
		
		for (Entry<Integer, Double> entry : qSumTerms.entrySet() ){
			weights.add(new Tuple<Integer, Double>(entry.getKey(),entry.getValue()));
		}
		return weights;
	}
}
