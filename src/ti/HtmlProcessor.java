package ti;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * A processor to extract terms from HTML documents.
 */
public class HtmlProcessor implements DocumentProcessor
{

	// P3
	 Hashtable<String, Integer> stopWords = new Hashtable<String, Integer>();

	/**
	 * Creates a new HTML processor.
	 *
	 * @param pathToStopWords the path to the file with stopwords, or {@code null} if stopwords are not filtered.
	 * @throws IOException if an error occurs while reading stopwords.
	 */
	public HtmlProcessor(File pathToStopWords) throws IOException
	{
		// P3
		// cargar stopwords
		if(pathToStopWords==null) return;
	    FileInputStream fis = null;
        BufferedReader br =null;

	    try {
	      fis = new FileInputStream(pathToStopWords);
	      br = new BufferedReader(new InputStreamReader(fis));
	      int counter = 0;
	      String line = "";
	      while ((line = br.readLine()) != null) {	    	  
	        stopWords.put(line, counter);
	        counter++;
	      }

	      // dispose all the resources after using them.
	      fis.close();
	      br.close();

	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    
	}

	/**
	 * {@inheritDoc}
	 */
	public Tuple<String, String> parse(String html){
		// P3
		// parsear documento
		Document doc = Jsoup.parse(html, "UTF-8"); 
		return new Tuple<String, String> (doc.title(),doc.text());
		
	}

	/**
	 * Process the given text (tokenize, normalize, filter stopwords and stemize) and return the list of terms to index.
	 *
	 * @param text the text to process.
	 * @return the list of index terms.
	 */
	public ArrayList<String> processText(String text)
	{
		ArrayList<String> terms = new ArrayList<>();
		ArrayList<String> tokens = new ArrayList<>();
		ArrayList<String> tokensNormalized = new ArrayList<>();
		// P3
		// tokenizar, normalizar, stopword, stem, etc.
		// tokenizar
		tokens = tokenize(text);
		// normalizar
		for (String token: tokens){
			tokensNormalized.add(normalize(token));
		}
		// stopword
		for (String term : tokensNormalized){
			if(!isStopWord(term)){
				terms.add(stem(term));
			}
		}
		return terms;
	}

	/**
	 * Tokenize the given text.
	 *
	 * @param text the text to tokenize.
	 * @return the list of tokens.
	 */
	protected ArrayList<String> tokenize(String text)
	{
		String [] words = text.toLowerCase().replaceAll("<[^>]+>", " ").replaceAll("[^a-z0-9']", " ").split("\\s+");
		//String [] words = text.split("\\s+");
		return  new ArrayList<String>(Arrays.asList(words));
	}

	/**
	 * Normalize the given term.
	 *
	 * @param text the term to normalize.
	 * @return the normalized term.
	 */
	protected String normalize(String text)
	{
		String normalized = null;

		// P3
		// hacemos el normalizado a la vez que hacemos el tokenizado, por si un termino tiene algo que queramos separar.
		//normalized = text.toLowerCase().replaceAll("<[^>]+>", " ").replaceAll("[^a-z0-9']", " ");

		return text;
	}

	/**
	 * Checks whether the given term is a stopword.
	 *
	 * @param term the term to check.
	 * @return {@code true} if the term is a stopword and {@code false} otherwise.
	 */
	protected boolean isStopWord(String term)
	{
		//boolean isTopWord = false;

		// P3

		return stopWords.containsKey(term);
	}

	/**
	 * Stem the given term.
	 *
	 * @param term the term to stem.
	 * @return the stem of the term.
	 */
	protected String stem(String term)
	{
		String stem = null;

		// P3
		Stemmer s = new Stemmer();
		char[] termChar = term.toCharArray();
		int i;
		for (i = 0; i<term.length();i++){
			if(Character.isLetter((char)termChar[i])) 
				return term;
			s.add(termChar[i]);
		}
		s.stem();
		stem = s.toString();

		return stem;
	}
}
