package ti;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * This class contains the logic to run the indexing process of the search engine.
 */
public class Indexer
{
    protected File pathToIndex;
    protected File pathToCollection;
    protected DocumentProcessor docProcessor;

	/**
     * Creates a new indexer with the given paths and document processor.
     * @param pathToIndex path to the index directory.
     * @param pathToCollection path to the original documents directory.
     * @param docProcessor document processor to extract terms.
     */
    public Indexer(File pathToIndex, File pathToCollection, DocumentProcessor docProcessor)
    {
        this.pathToIndex = pathToIndex;
        this.pathToCollection = pathToCollection;
        this.docProcessor = docProcessor;
    }

	/**
     * Run the indexing process in two passes and save the index to disk.
     * @throws IOException if an error occurs while indexing.
     */
    public void run() throws IOException
    {
        Index ind = new Index(this.pathToIndex.getPath());
        this.firstPass(ind);
        this.secondPass(ind);

        // Save index
        System.err.print("Saving index...");
        ind.save();
        System.err.println("done.");
        System.err.println("Index statistics:");
        ind.printStatistics();
    }
    /**
     * Runs the first pass of the indexer.
     * It builds the inverted index by iterating all original document files and calling {@link #processDocument}.
     * @param ind the index.
     * @throws IOException if an error occurs while processing a document.
     */
    protected void firstPass(Index ind) throws IOException
    {
        DecimalFormat df = new DecimalFormat("#.##");
        long startTime = System.currentTimeMillis();
        int totalDocuments = 0;
        long totalBytesDocuments = 0;

        System.err.println("Running first pass...");
        for (File subDir : this.pathToCollection.listFiles()) {
            if (!subDir.getName().startsWith(".")) {
                for (File docFile : subDir.listFiles()) {
                    if (docFile.getPath().endsWith(".html")) {
                        try {
                            System.err.print("  Indexing file " + docFile.getName() + "...");
                            this.processDocument(docFile, ind);
                            System.err.print("done.");
                        } catch (IOException ex) {
                            System.err.println("exception!");
                            System.err.print(ex.getMessage());
                        } finally {
                            System.err.println();
                        }
                        totalDocuments++;
                        totalBytesDocuments += docFile.length();
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        double totalTime = (endTime - startTime) / 1000d;
        double totalMegabytes = totalBytesDocuments / 1024d / 1024d;
        System.err.println("...done:");
        System.err.println("  - Documents: " + totalDocuments + " (" + df.format(totalMegabytes) + " MB).");
        System.err.println("  - Time: " + df.format(totalTime) + " seconds.");
        System.err.println("  - Throughput: " + df.format(totalMegabytes / totalTime) + " MB/s.");
    }
    /**
     * Runs the second pass of the indexer.
     * Here it traverses the inverted index to compute and store IDF, update weights in the postings,
     * build the direct index, and compute document norms.
     * @param ind the index.
     */
    protected void secondPass(Index ind)
    {
        DecimalFormat df = new DecimalFormat("#.##");
        long startTime = System.currentTimeMillis();
        System.err.println("Running second pass...");
        System.err.println("  Updating term weights and direct index...");

		// P2
		// recorrer el índice para calcular IDF y actualizar pesos
        // iterar por el vocabulario

        // recorrer el i­ndice para calcular IDF y actualizar pesos
        // iterar por el termID en el vocabulario
          // agarrar del inverted en cuantos docs aparece el termino = ct
          // actulizamos idf en el vocabulary:
          // sabiendo que nd(num docs total)=ind.documents.size();, y teniendo ct, idf=log(1+nd/ct)
          // actualizamos inverted:
         // multiplicamos tf(ya lo tenemos) por idf = peso
     	   // precalc norma: vamos sumando los pesos del termino en cada doc en un array donde docid=posicion en el array
             //calculo normas: iterando en el array de normas por docId, aplicamos sqrt i guardamos norma en el documents.

        int nd = ind.documents.size();
        for( Map.Entry<String, Tuple<Integer,Double>> entry : ind.vocabulary.entrySet() ){
        	int termID = entry.getValue().item1;
        	//para cada termino, vamos a ver en cuantos documentos aparacere. Esta será nuestra lista de posting
        	ArrayList<Tuple<Integer, Double>> posting = ind.invertedIndex.get(termID);
        	int ct = posting.size();
        	double idf = Math.log(1.0+(double)((double) nd/(double) ct));
        	entry.getValue().item2 = idf;
        	
        	//calculo y actualización de la norma norma
            for( Tuple<Integer,Double> elementoPosting : posting ){
            	elementoPosting.item2 =elementoPosting.item2*idf;
            	//redor esto
//        		ind.documents.get(elementoPosting.item1).item2 += Math.sqrt(elementoPosting.item2*elementoPosting.item2);
        		ind.documents.get(elementoPosting.item1).item2 += (elementoPosting.item2*elementoPosting.item2);
        		
                // P4 - En secondPass deben incorporarse los términos con sus pesos correspondientes.
        		// actualizar directIndex
        		Tuple<Integer, Double> termsToAddInDirectIndex = new Tuple<Integer,Double>(termID,elementoPosting.item2);
        		ind.directIndex.get(elementoPosting.item1).add(termsToAddInDirectIndex);
        	}
        }
                
        System.err.println("done.");
        System.err.print("  Updating document norms...");

		// P2
		// actualizar normas de documentos
        //hacer otro for para hacer sqrt 
        long endTime = System.currentTimeMillis();
        double totalTime = (endTime - startTime) / 1000d;
        System.err.println("done.");
        System.err.println("...done");
        System.err.println("  - Time: " + df.format(totalTime) + " seconds.");
    }
	/**
     * Process the original document in the specified path and add it to the given index.
     * <p>
     * After extracting the document terms, it populates the vocabulary and document structures,
     * and adds the corresponding postings to the inverted index.
     * @param docFile the path to the original document file.
     * @param ind the index to add the document to.
     * @throws IOException if an error occurrs while processing this document.
     */
    protected void processDocument(File docFile, Index ind) throws IOException
    {
		// P2
    	// TENEMOS QUE PARSEAR EL TEXTO primero
    	//parseas pillas el titulo, 
    	//pillas el body 
    	//y los juntas los terminos
    	String docName = docFile.getName().substring(0, docFile.getName().lastIndexOf("."));
//    	System.out.println("\n\tDOC name "+docName);
		ind.documents.add(new Tuple<String, Double>(docName,0.0));
		
        // leer documento desde disco
    	String docInString = readFile(docFile.getPath(), StandardCharsets.UTF_8);
		// procesarlo para obtener los términos
    	
    	Tuple<String, String> currentDoc = docProcessor.parse(docInString); 
    	
    	
    	ArrayList<String> titleTerms = docProcessor.processText(currentDoc.item1);
    	ArrayList<String> bodyTerms = docProcessor.processText(currentDoc.item2);
    	
    	ArrayList<String> terms = new ArrayList<String>();
    	terms.addAll(titleTerms);
    	terms.addAll(bodyTerms);
    	
    	Set <String> set = new HashSet<String>(terms);
    	int docID = ind.documents.size()-1;
    	for (String term :set){
    		int freq = Collections.frequency(terms, term);
    		Tuple<Integer, Double> termToCheckInVoc = ind.vocabulary.get(term);
    		if(termToCheckInVoc == null ){
    			//Si entramos aqui es por que no existe el termino en el vocabulario, estamos añadiendo (termID, iDF) que lo ponemos a 0 por el momento
    			termToCheckInVoc = new Tuple<Integer,Double>(ind.vocabulary.size(),0.0);
    			ind.vocabulary.put(term, termToCheckInVoc);

    			ArrayList<Tuple<Integer, Double>> termToAddInInvertedIndex = new ArrayList<Tuple<Integer,Double>>();
    			termToAddInInvertedIndex.add(new Tuple<Integer,Double>(docID,1+Math.log(freq)));
        		ind.invertedIndex.add(termToAddInInvertedIndex);    				
    		}else{
       			//Si entramos aqui es por que YA existe el termino en el vocabulario
        		ind.invertedIndex.get(ind.vocabulary.get(term).item1).add(new Tuple<Integer,Double>(docID,1+Math.log(freq)));				       			
       		}
    	}
		// actualizar estructuras del índice: vocabulary, documents e invertedIndex

    	//p4 - En processDocument debe inicializarse el postings list del documento.
		ArrayList<Tuple<Integer, Double>> docToAddInDirectIndex = new ArrayList<Tuple<Integer,Double>>();
		ind.directIndex.add(docToAddInDirectIndex);    				
		
		//añadimos a caché
		ind.setCachedDocument(docID, new Tuple<String,String>(currentDoc.item1,currentDoc.item2));
    }
    static String readFile(String path, Charset encoding) throws IOException {
    	byte[] encoded = Files.readAllBytes(Paths.get(path));
    	return new String(encoded, encoding);
    }
}
